//
//  CategoryStruct.swift
//  
//
//  Created by Ирина on 26/02/2019.
//

import Foundation

struct CategoryStruct: Decodable {
    
    var image: String?
    var id: Int?
    var parent_id: Int?
    var name: String?
}
