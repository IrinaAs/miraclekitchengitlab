//
//  ListDetailRecipeStruct.swift
//  miracleKitchen
//
//  Created by Ирина on 05/03/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import Foundation

struct ListDetailRecipeStruct: Decodable {
    
    var image: String?
    var id: Int?
    var name: String?
    var categoryId: Int?
    var level: String?
    
}
