//
//  ViewController.swift
//  miracleKitchen
//
//  Created by Ирина on 20/02/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class CategoryViewController: UIViewController {
    
    @IBOutlet weak var tableCategory: UITableView!
    
    private let identificatorCell = "CellCategory"
    
    var massCategory: [CategoryStruct] = []
    
    let urlCategory = "http://app-recipes.getsandbox.com/categories"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableCategory.dataSource = self
        configureNavigationBar()
        getCategoryJson()
        
    }
    
    fileprivate func getCategoryJson() {
        Alamofire.request("http://app-recipes.getsandbox.com/categories").responseData { response in
            
            if let json = response.result.value {
                print("JSON: \(json)")
                
                self.massCategory = try! JSONDecoder().decode([CategoryStruct].self, from: json )
                
                print(self.massCategory)
                print(json)
                
                self.tableCategory.reloadData()
            }
        }
    }
    
    fileprivate func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.4819767475, green: 0.3423785567, blue: 0.3682767153, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9663766026, green: 0.9972212911, blue: 1, alpha: 1), NSAttributedString.Key.font: UIFont(name: "Savoye LET", size: 38)!]
        tableCategory.backgroundColor = #colorLiteral(red: 0.4819767475, green: 0.3423785567, blue: 0.3682767153, alpha: 1)
    }
}

extension CategoryViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return massCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identificatorCell, for: indexPath) as! CategoryTableViewCell
        
        let informationCategory = massCategory[indexPath.row]

//      Загрузка картинка по URL c формата JSON
        let downloadURL = URL(string: informationCategory.image!)!
        cell.categoryImage.af_setImage(withURL: downloadURL)
        
        cell.mainCategoryLabel.text = informationCategory.name
        
        cell.tag = informationCategory.id!
        
        print(cell.tag)
        
        return cell
        
    }
    
    //- Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        
        if segue.identifier == "subSegue" {
            
            let vcSub = segue.destination as! SubCategoryViewController
            
            if let cell = sender as? CategoryTableViewCell {
                
                vcSub.navigationItem.title = cell.mainCategoryLabel.text
                
                vcSub.idTagSub = cell.tag
                
            }
        }
    }
}







