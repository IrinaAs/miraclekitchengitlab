//
//  СategoriesDishesTableViewCell.swift
//  miracleKitchen
//
//  Created by Ирина on 20/02/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var mainCategoryLabel: UILabel!
    
    @IBOutlet weak var categoryImage: UIImageView!
    

}
