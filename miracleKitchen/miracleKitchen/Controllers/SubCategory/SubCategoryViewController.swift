//
//  SubCategoryViewController.swift
//  miracleKitchen
//
//  Created by Ирина on 27/02/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit
import Alamofire

class SubCategoryViewController: UIViewController {
    
    @IBOutlet weak var tableSubCategory: UITableView!
    
    private let identifierCell = "CellSubCategory"
    
    var subCategory: [CategoryStruct] = []
    var filterSubCategory: [CategoryStruct] = []
    
    var idTagSub: Int = 0
    

    let searchController = UISearchController (searchResultsController: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableSubCategory.backgroundColor = #colorLiteral(red: 0.4819767475, green: 0.3423785567, blue: 0.3682767153, alpha: 1)
        
        //        Вызов функции setUpSearchBar().Изменение кнопки Cancell  на иконку
        setUpSearchBar()
        
        //        Убрали разделение пустых ячеек
        self.tableSubCategory.tableFooterView = UIView()
        
        //       Передача с API данные в формате JSON
        getSubCategoryJson()
        
        //       Настройка searchController
        tuningPresentationSearchController()
        
        self.tableSubCategory.dataSource = self
        self.tableSubCategory.delegate = self
        searchController.searchBar.delegate = self
        
        //      Убираем searchController сдвигом таблицы вверх
        self.edgesForExtendedLayout = .bottom
        
        //      Изменяем внешний вид searchController
        let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField
        let backgroundview = textfield?.subviews.first
        backgroundview?.backgroundColor = UIColor.white
        backgroundview?.layer.cornerRadius = 10
        backgroundview?.clipsToBounds = true
        
    }
    
    //    override func viewDidLayoutSubviews() {
    //        super.viewDidLayoutSubviews()
    //        searchController.searchBar.showsCancelButton = false
    //        searchController.hidesNavigationBarDuringPresentation = false
    //    }
    
    fileprivate func getSubCategoryJson() {
        Alamofire.request("http://app-recipes.getsandbox.com/categories/\(idTagSub)").responseData { response in
            
            if let json = response.result.value {
                print("JSON: \(json)")
                
                self.subCategory = try! JSONDecoder().decode([CategoryStruct].self, from: json )
                
                print(self.subCategory)
                print(json)
                
                self.tableSubCategory.reloadData()
            }
        }
    }
    
    fileprivate func tuningPresentationSearchController() {
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск блюда"
        searchController.searchBar.barTintColor = UIColor.lightGray
        searchController.searchBar.tintColor = #colorLiteral(red: 0.4819767475, green: 0.3423785567, blue: 0.3682767153, alpha: 1)
        navigationItem.searchController = searchController
    }
    
    fileprivate func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        searchController.searchBar.subviews[0].subviews[0].backgroundColor = #colorLiteral(red: 0.9687401652, green: 0.8759450912, blue: 0.8997306228, alpha: 1)
        
    }
    
    fileprivate func filterContentForSearchText( _ searchText: String, scope: String = "All") {
        
        filterSubCategory = subCategory.filter({(category: CategoryStruct) -> Bool in
            
            return (category.name?.lowercased().contains(searchText.lowercased()))!
        })
        
        self.tableSubCategory.reloadData()
    }
    
    fileprivate func  isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    
    fileprivate func setUpSearchBar() {
        
        let barButtonAppearanceInSearchBar: UIBarButtonItem?
        barButtonAppearanceInSearchBar = UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        barButtonAppearanceInSearchBar?.image = UIImage(named: "close.png")?.withRenderingMode(.alwaysTemplate)
        barButtonAppearanceInSearchBar?.tintColor = UIColor.white
        barButtonAppearanceInSearchBar?.title = nil
    }
}



extension SubCategoryViewController: UITableViewDelegate {
    
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.showsCancelButton = false
    }
    
}

extension SubCategoryViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering() {
            return filterSubCategory.count
        }
        return subCategory.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierCell, for: indexPath) as! SubCategoryTableViewCell
        
        let category: CategoryStruct
        
        if isFiltering() {
            category = filterSubCategory[indexPath.row]
        } else {
            category = subCategory[indexPath.row]
        }
        
        cell.subCategoryLabel.text = category.name
        
        cell.tag = subCategory[indexPath.row].id!
        
        print(cell.tag)
        
        return cell
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        
        if segue.identifier == "ListDetailRecipe" {
            
            let vcListDetail = segue.destination as! ListDetailRecipeViewController
            
            if let cell = sender as? SubCategoryTableViewCell {
                
                vcListDetail.navigationItem.title = cell.subCategoryLabel.text
                
                vcListDetail.idTagListDetail = cell.tag
               
                
            }
        }
    }
}


extension SubCategoryViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

extension SubCategoryViewController: UISearchResultsUpdating{
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
}

