//
//  ChoiceSubCategoryViewController.swift
//  miracleKitchen
//
//  Created by Ирина on 04/03/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit
import Alamofire

class ListDetailRecipeViewController: UIViewController {
    
    
    @IBOutlet weak var tableViewListDetail: UITableView!
    
    private let identificatorCell = "CellListDetail"
    
    var massListDetailRecipe: [ListDetailRecipeStruct] = []
    var massFilterListDetailRecipe: [ListDetailRecipeStruct] = []
    
    var idTagListDetail: Int = 0
    
    
    let searchController = UISearchController (searchResultsController: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewListDetail.backgroundColor = #colorLiteral(red: 0.4819767475, green: 0.3423785567, blue: 0.3682767153, alpha: 1)
        self.tableViewListDetail.dataSource = self
        //        Убрали разделение пустых ячеек
        self.tableViewListDetail.tableFooterView = UIView()
        
        //        Вызов функции setUpSearchBar().Изменение кнопки Cancell  на иконку
        setUpSearchBar()
        
        //       Передача с API данные в формате JSON
        getSubCategoryJson()
        
        //       Настройка searchController
        tuningPresentationSearchController()
        
        searchController.searchBar.delegate = self
        
        //      Убираем searchController сдвигом таблицы вверх
        self.edgesForExtendedLayout = .bottom
        
        //      Изменяем внешний вид searchController
        let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField
        let backgroundview = textfield?.subviews.first
        backgroundview?.backgroundColor = UIColor.white
        backgroundview?.layer.cornerRadius = 10
        backgroundview?.clipsToBounds = true
    }
    
    fileprivate func getSubCategoryJson() {
        Alamofire.request("http://app-recipes.getsandbox.com/recipes/\(idTagListDetail)").responseData { response in
            
            if let json = response.result.value {
                print("JSON: \(json)")
                
                self.massListDetailRecipe = try! JSONDecoder().decode([ListDetailRecipeStruct].self, from: json )
                
                print(self.massListDetailRecipe)
                print(json)
                
                self.tableViewListDetail.reloadData()
            }
        }
    }
    
    
    fileprivate func tuningPresentationSearchController() {
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск блюда"
        searchController.searchBar.barTintColor = UIColor.lightGray
        searchController.searchBar.tintColor = #colorLiteral(red: 0.4819767475, green: 0.3423785567, blue: 0.3682767153, alpha: 1)
        navigationItem.searchController = searchController
    }
    
    fileprivate func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        searchController.searchBar.subviews[0].subviews[0].backgroundColor = #colorLiteral(red: 0.9687401652, green: 0.8759450912, blue: 0.8997306228, alpha: 1)
        
    }
    
    fileprivate func filterContentForSearchText( _ searchText: String, scope: String = "All") {
        
        massFilterListDetailRecipe = massListDetailRecipe.filter({(recipe: ListDetailRecipeStruct) -> Bool in
            
            return (recipe.name?.lowercased().contains(searchText.lowercased()))!
        })
        
        self.tableViewListDetail.reloadData()
    }
    
    fileprivate func  isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    
    fileprivate func setUpSearchBar() {
        let barButtonAppearanceInSearchBar: UIBarButtonItem?
        barButtonAppearanceInSearchBar = UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        barButtonAppearanceInSearchBar?.image = UIImage(named: "close.png")?.withRenderingMode(.alwaysTemplate)
        barButtonAppearanceInSearchBar?.tintColor = UIColor.white
        barButtonAppearanceInSearchBar?.title = nil
    }
}

extension ListDetailRecipeViewController: UITableViewDelegate {
    
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.showsCancelButton = false
    }
}

extension ListDetailRecipeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering() {
            return massFilterListDetailRecipe.count
        }
        return massListDetailRecipe.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identificatorCell, for: indexPath) as? ListDetailRecipeTableViewCell
        
        let recipe: ListDetailRecipeStruct
        
        if isFiltering() {
            recipe = massFilterListDetailRecipe[indexPath.row]
        } else {
            recipe = massListDetailRecipe[indexPath.row]
        }
        
        cell!.titleListDetailLabel.text = recipe.name
        
        //        let downloadURL = URL(string: informationListDetail.image!)!
        //        cell!.ListDetailImageView.af_setImage(withURL: downloadURL)
        
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        
        if segue.identifier == "DetailRecipe" {
            
            let vcDetail = segue.destination as! DetailRecipeViewController
            
            if let cell = sender as? ListDetailRecipeTableViewCell {
                
//                idTagListDetail.navigationItem.title = cell.subCategoryLabel.text
//
//                idTagListDetail.idTagListDetail = cell.tag
                
            }
        }
    }
}

extension ListDetailRecipeViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText (searchController.searchBar.text!)
    }
}

extension ListDetailRecipeViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText (searchController.searchBar.text!)
    }
    
}
