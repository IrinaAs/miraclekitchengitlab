//
//  ChoiceSubCategoryTableViewCell.swift
//  miracleKitchen
//
//  Created by Ирина on 04/03/2019.
//  Copyright © 2019 Ирина. All rights reserved.
//

import UIKit

class ListDetailRecipeTableViewCell: UITableViewCell {

    @IBOutlet weak var ListDetailImageView: UIImageView!
    @IBOutlet weak var titleListDetailLabel: UILabel!
    @IBOutlet weak var levelListDetailLabel: UILabel!
    @IBOutlet weak var timeListDetailLabel: UILabel!
    
}
